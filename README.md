# ActivityRouter

## 项目介绍

- 项目名称: ActivityRouter
- 所属系列：openharmony的第三方组件适配移植
- 功能：支持给Ability定义 URL，这样可以通过 URL 跳转到Ability，支持在浏览器以及 app 中跳入。
- 项目移植状态: 主功能完成
- 调用差异: 无
- 开发版本：sdk6，DevEco Studio 2.2 Beta1
- 基线版本：Releases v1.1.5

## 项目演示
![输入图片说明](https://gitee.com/chinasoft_ohos/ActivityRouter/raw/master/img/demo1.gif "demo1.gif")

## 安装教程

1.在项目根目录下的build.gradle文件中，
 ```
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
}
 ```

2.在entry模块的build.gradle文件中，
```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
    
    implementation('com.gitee.chinasoft_ohos:ActivityRouter-annotation:1.0.0')
    implementation('com.gitee.chinasoft_ohos:ActivityRouter-app_module:1.0.0')
    implementation('com.gitee.chinasoft_ohos:ActivityRouter-compiler:1.0.0')
    annotationProcessor('com.gitee.chinasoft_ohos:ActivityRouter-compiler:1.0.0')
    implementation('com.gitee.chinasoft_ohos:ActivityRouter-activityrouter:1.0.0')
	……
}
```
在sdk6，DevEco Studio 2.2 Beta1下项目可直接运行 如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件 并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

## 使用说明
1.在需要配置的Ability上添加注解

``` java
@Router("main")
public class MainAbility extends Ability {
	...
}
```
这样就可以通过mzule://main来打开MainAbility了。


2.配置多个地址

``` java
@Router({"main", "root"})
```

这样mzule://main和mzule://root都可以访问到同一个Ability


3.添加Callback

``` java
public class MyApplication extends AbilityPackage implements RouterCallbackProvider {

    @Override
    public void onInitialize() {
        super.onInitialize();

    }

    @Override
    public RouterCallback provideRouterCallback() {
        return new SimpleRouterCallback(){

            @Override
            public boolean beforeOpen(Context context, Uri uri) {
                if (uri.toString().startsWith("mzule://")){
                    Intent intent = new Intent();
                    Operation operationBuilder = new Intent.OperationBuilder()
                            .withBundleName(context.getBundleName())
                            .withAbilityName(LaunchAbility.class.getName())
                            .build();
                    intent.setOperation(operationBuilder);
                    context.startAbility(intent,11);
                    return true;
                }
                return false;
            }

            @Override
            public void notFound(Context context, Uri uri) {
                Intent intent = new Intent();
                Operation operation =new Intent.OperationBuilder()
                        .withBundleName(context.getBundleName())
                        .withFlags(Intent.FLAG_ABILITY_NEW_MISSION)
                        .withAbilityName(NotFoundAbility.class.getName())
                        .build();
                intent.setOperation(operation);
                context.startAbility(intent,2);
            }

            @Override
            public void error(Context context, Uri uri, Throwable e) {
                Intent intent1 = new Intent();
                Operation operation =new Intent.OperationBuilder()
                        .withBundleName(context.getBundleName())
                        .withFlags(Intent.FLAG_ABILITY_NEW_MISSION)
                        .withAbilityName(ErrorStackAbility.class.getName())
                        .build();
                intent1.setParam("uri",uri);
                intent1.setParam("error",e);
                intent1.setOperation(operation);
                context.startAbility(intent1,3);
            }
        };
    }
}
```
在AbilityPackage中实现RouterCallbackProvider接口，通过provideRouterCallback()方法提供RouterCallback，具体 API 如上。

4.支持 Http(s) 协议

``` java
@Router({"http://mzule.com/main", "main"})
```
5.支持通过 url 调用方法

``` java
@Router("logout")
public static void logout(Context context, IntentParams bundle) {
    ...
}
```
在任意参数为 Context 和 IntentParams 的静态公共方法上, 通过 @Router 标记即可定义方法的 url. @Router 使用方式与上述一致。

## 测试信息
CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

## 版本迭代
- 1.0.0

## 版权和许可信息
- Apache License 2.0