package com.github.mzule.abilityrouter.router;

import ohos.utils.net.Uri;
/**
 * Created by CaoDongping on 4/7/16.
 */
public class Path {
    private final String value;
    private Path next;

    private Path(String value) {
        this.value = value;
    }
    /**
     * 路径匹配方法
     *
     *  @param format 格式
     *  @param link 链接
     * @return match 匹配
     */
    public static boolean match(final Path format, final Path link) {
        if (format == null || link == null) {
            return false;
        }
        if (format.length() != link.length()) {
            return false;
        }
        Path xt = format;
        Path ya = link;
        while (xt != null) {
            if (!xt.match(ya)) {
                return false;
            }
            xt = xt.next;
            ya = ya.next;
        }
        return true;
    }
    /**
     * 创建
     *
     *  @param uri URI地址
     * @return create 创建
     */
    public static Path create(Uri uri) {
        Path path = new Path(uri.getScheme().concat("://"));
        String urlPath = uri.getDecodedPath();
        if (urlPath == null) {
            urlPath = "";
        }
        if (urlPath.endsWith("/")) {
            urlPath = urlPath.substring(0, urlPath.length() - 1);
        }
        parse(path, uri.getDecodedHost() + urlPath);
        return path;
    }

    private static void parse(Path scheme, String s) {
        String[] components = s.split("/");
        Path curPath = scheme;
        for (String component : components) {
            Path temp = new Path(component);
            curPath.next = temp;
            curPath = temp;
        }
    }
    /**
     * 定义一个获取下一个内容的方法
     *
     * @return next 下一个
     */
    public Path next() {
        return next;
    }
    /**
     * 定义一个长度的方法
     *
     * @return length 长度
     **/
    public int length() {
        Path path = this;
        int len = 1;
        while (path.next != null) {
            len++;
            path = path.next;
        }
        return len;
    }

    private boolean match(Path path) {
        return isArgument() || value.equals(path.value);
    }

    public boolean isArgument() {
        return value.startsWith(":");
    }
    /**
     * 论据的方法
     *
     * @return argument 论据
     */
    public String argument() {
        return value.substring(1);
    }
    /**
     * 定义一个获取value的方法
     *
     * @return value 类型
     */
    public String value() {
        return value;
    }
    /**
     * 定义一个判断是否是http的方法
     *
     * @return isHttp 是http
     */
    public boolean isHttp() {
        String low = value.toLowerCase();
        return low.startsWith("http://") || low.startsWith("https://");
    }

}
