package com.github.mzule.abilityrouter.router;

/**
 * Created by CaoDongping on 4/8/16.
 */
public interface RouterCallbackProvider {
    /**
     * 提供路由器回调
     *
     * @return RouterCallback 回调
     * */
    RouterCallback provideRouterCallback();
}
