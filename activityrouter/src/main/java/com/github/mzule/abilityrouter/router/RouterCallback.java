package com.github.mzule.abilityrouter.router;

import ohos.app.Context;
import ohos.utils.net.Uri;
/**
 * Created by CaoDongping on 4/8/16.
 */
public interface RouterCallback {
    /**
     * 定义一个未找到的方法
     *
     * @param context 上下文
     * @param uri URI地址
    */
    void notFound(Context context, Uri uri);
    /**
     * 定义一个打开前的方法
     *
     * @param context 上下文
     * @param uri URI地址
     * @return beforOpen 打开前
     */
    boolean beforeOpen(Context context, Uri uri);
    /**
     * 定义一个打开后的方法
     *
     * @param context 上下文
     * @param uri URI地址
     */
    void afterOpen(Context context, Uri uri);
    /**
     * 定义一个错误的方法
     *
     * @param context 上下文
     * @param uri URI地址
     * @param e 错误
     */
    void error(Context context, Uri uri, Throwable e);
}
