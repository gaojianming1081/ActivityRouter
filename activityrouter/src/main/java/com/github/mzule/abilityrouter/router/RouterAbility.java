/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.github.mzule.abilityrouter.router;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.utils.net.Uri;
/**
 * Created by CaoDongping on 4/6/16.
 */
public class RouterAbility extends Ability {
    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        RouterCallback callback = getRouterCallback();
        Uri uri = getIntent().getUri();
        if (uri != null) {
            Routers.open(this, uri, callback);
        }

        terminateAbility();
    }

    private RouterCallback getRouterCallback() {
        if (getAbilityPackage() instanceof RouterCallbackProvider) {
            return ((RouterCallbackProvider) getAbilityPackage()).provideRouterCallback();
        }
        return null;
    }
}
