package com.github.mzule.abilityrouter.router;

import java.util.Map;
/**
 * Created by CaoDongping on 4/6/16.
 */
public class ExtraTypes {
    /**
     * 定义一个string类型的变量
     * */
    public static final int STRING = -1;
    /**
     * 定义一个int类型的变量
     * */
    public static final int INT = 1;
    /**
     * 定义一个long类型的变量
     * */
    public static final int LONG = 2;
    /**
     * 定义一个bool类型的变量
     * */
    public static final int BOOL = 3;
    /**
     * 定义一个short类型的变量
     * */
    public static final int SHORT = 4;
    /**
     * 定义一个float类型的变量
     * */
    public static final int FLOAT = 5;
    /**
     * 定义一个布尔类型的变量
     * */
    public static final int DOUBLE = 6;
    /**
     * 定义一个byte类型的变量
     * */
    public static final int BYTE = 7;
    /**
     * 定义一个char类型的变量
     * */
    public static final int CHAR = 8;
    private String[] intExtra;
    private String[] longExtra;
    private String[] booleanExtra;
    private String[] shortExtra;
    private String[] floatExtra;
    private String[] doubleExtra;
    private String[] byteExtra;
    private String[] charExtra;
    private Map<String, String> transfer;

    public String[] getIntExtra() {
        String[] intExtra = this.intExtra;
        return intExtra;
    }

    public void setIntExtra(String[] intExtra) {
        this.intExtra = intExtra.clone();
    }

    public String[] getLongExtra() {
        String[] longExtra = this.longExtra;
        return longExtra;
    }

    public void setLongExtra(String[] longExtra) {
        this.longExtra = longExtra.clone();
    }

    public String[] getBooleanExtra() {
        String[] booleanExtra = this.booleanExtra;
        return booleanExtra;
    }

    public void setBooleanExtra(String[] booleanExtra) {
        this.booleanExtra = booleanExtra.clone();
    }

    public String[] getShortExtra() {
        String[] shortExtra = this.shortExtra;
        return shortExtra;
    }

    public void setShortExtra(String[] shortExtra) {
        this.shortExtra = shortExtra.clone();
    }

    public String[] getFloatExtra() {
        String[] floatExtra = this.floatExtra;
        return floatExtra;
    }

    public void setFloatExtra(String[] floatExtra) {
        this.floatExtra = floatExtra.clone();
    }

    public String[] getDoubleExtra() {
        String[] doubleExtra = this.doubleExtra;
        return doubleExtra;
    }

    public void setDoubleExtra(String[] doubleExtra) {
        this.doubleExtra = doubleExtra.clone();
    }

    public String[] getByteExtra() {
        String[] byteExtra = this.byteExtra;
        return byteExtra;
    }

    public void setByteExtra(String[] byteExtra) {
        this.byteExtra = byteExtra.clone();
    }

    public String[] getCharExtra() {
        String[] charExtra = this.charExtra;
        return charExtra;
    }

    public void setCharExtra(String[] charExtra) {
        this.charExtra = charExtra.clone();
    }

    public Map<String, String> getTransfer() {
        Map<String, String> transfer = this.transfer;
        return transfer;
    }

    public void setTransfer(Map<String, String> transfer) {
        this.transfer = transfer;
    }
    /**
     * 获取类型
     *
     *  @param  name 类型名字
     *  @return getType 获取不同的类型参数
     */
    public int getType(String name) {
        if (arrayContain(intExtra, name)) {
            return INT;
        }
        if (arrayContain(longExtra, name)) {
            return LONG;
        }
        if (arrayContain(booleanExtra, name)) {
            return BOOL;
        }
        if (arrayContain(shortExtra, name)) {
            return SHORT;
        }
        if (arrayContain(floatExtra, name)) {
            return FLOAT;
        }
        if (arrayContain(doubleExtra, name)) {
            return DOUBLE;
        }
        if (arrayContain(byteExtra, name)) {
            return BYTE;
        }
        if (arrayContain(charExtra, name)) {
            return CHAR;
        }
        return STRING;
    }

    private static boolean arrayContain(String[] array, String value) {
        if (array == null) {
            return false;
        }
        for (String s1 : array) {
            if (s1.equals(value)) {
                return true;
            }
        }
        return false;
    }
    /**
     * 转移
     *
     *  @param  name 类型名字
     *  @return transfer 传递类型名字
     */
    public String transfer(String name) {
        if (transfer == null) {
            return name;
        }
        String result = transfer.get(name);
        return result != null ? result : name;
    }
}
