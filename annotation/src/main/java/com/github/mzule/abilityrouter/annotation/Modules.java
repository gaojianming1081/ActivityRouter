package com.github.mzule.abilityrouter.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by CaoDongping on 30/10/2016.
 */
@Retention(RetentionPolicy.CLASS)
public @interface Modules {
    /**
     * 类型
     *
     * @return value 类型
     * */
    String[] value();
}
