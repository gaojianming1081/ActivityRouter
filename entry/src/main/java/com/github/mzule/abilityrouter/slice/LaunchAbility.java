package com.github.mzule.abilityrouter.slice;

import com.github.mzule.abilityrouter.Constant;
import com.github.mzule.abilityrouter.ResourceTable;
import com.github.mzule.abilityrouter.router.Routers;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.ScrollView;
import ohos.agp.components.Text;
import ohos.agp.utils.Color;
import ohos.agp.utils.TextTool;
import ohos.agp.window.dialog.ToastDialog;
/**
 * Created by CaoDongping on 4/7/16.
 */
public class LaunchAbility extends Ability {
    /**
     * 定义一个 用于接收的变量
     */
    public static final int RESULT_OK = -1;
    private final int direction = 1;
    private final int scrollbarthickness = 12;
    private static final int MIN_DELAY_TIME = 1000;
    private static long lastClickTime;
    /**
     * 限制按钮多次点击一秒之内不能重复点击
     *
     * @return 是否快速点击
     **/
    public static boolean isFastClick() {
        boolean flag = true;
        long currentClickTime = System.currentTimeMillis();
        if ((currentClickTime - lastClickTime) >= MIN_DELAY_TIME) {
            flag = false;
        }
        lastClickTime = currentClickTime;
        return flag;
    }
    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_launch);

        ComponentContainer container = (ComponentContainer) findComponentById(ResourceTable.Id_container);
        ScrollView scrollView = (ScrollView) findComponentById(ResourceTable.Id_scroll);
        scrollView.enableScrollBar(direction,true);
        scrollView.setScrollbarColor(Color.GRAY);
        scrollView.setScrollbarThickness(scrollbarthickness);
        for (int i = 0; i < container.getChildCount(); i++) {
            final Component view = container.getComponentAt(i);
            if (view instanceof Text) {
                view.setClickedListener(new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        if (isFastClick()) {
                            return;
                        }
                        Routers.openForResult(LaunchAbility.this,((Text)view).getText(), Constant.REQUEST_CODE_DEMO);
                    }
                });
            }
        }
    }


    @Override
    protected void onAbilityResult(int requestCode, int resultCode, Intent resultData) {
        super.onAbilityResult(requestCode, resultCode, resultData);
        if (resultCode == RESULT_OK && requestCode == Constant.REQUEST_CODE_DEMO) {
            String msg;
            if (resultData == null) {
                msg = "success";
            } else {
                msg = resultData.getStringParam("msg");
                msg = TextTool.isNullOrEmpty(msg) ? "success" : msg;
            }
            ToastDialog toastDialog = new ToastDialog(this);
            toastDialog.setText(msg).show();
        }
    }
}
