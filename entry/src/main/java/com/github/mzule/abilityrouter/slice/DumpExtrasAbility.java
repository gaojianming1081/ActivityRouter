package com.github.mzule.abilityrouter.slice;

import com.github.mzule.abilityrouter.ResourceTable;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.IntentParams;
import ohos.agp.components.Text;
import java.util.Set;

/**
 * Created by CaoDongping on 4/7/16.
 */
public abstract class DumpExtrasAbility extends Ability {
    private static final int PADDINGS = 16;
    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_dump);

        Text text = (Text) findComponentById(ResourceTable.Id_dump);
        Intent bundle = getIntent();
        IntentParams params = bundle.getParams();

        if (params != null) {

            Set<String> strings = params.keySet();

            int padding = PADDINGS;
            text.setPadding(padding,padding,padding,padding);
            text.setText(getClass().getSimpleName());
            text.append("\n\n");

            for (String key : strings) {
                if ("callerBundleName".equals(key)) {
                    continue;
                }
                text.append(key + "=>");

                Object v1 = params.getParam(key);

                if (null != v1) {
                    text.append(v1 + "=>" + v1.getClass().getSimpleName());
                } else {
                    text.append("null");
                }

                text.append("\n\n");
            }

        }
    }
}
