package com.github.mzule.abilityrouter.slice;

import com.github.mzule.abilityrouter.annotation.Router;
/**
 * Created by CaoDongping on 4/7/16.
 */
@Router({"user/:userId", "user/:nickname/city/:city/gender/:gender/age/:age"})
public class UserAbility extends DumpExtrasAbility {
}
