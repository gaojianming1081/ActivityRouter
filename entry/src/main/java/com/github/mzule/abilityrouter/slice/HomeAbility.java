package com.github.mzule.abilityrouter.slice;

import com.github.mzule.abilityrouter.annotation.Router;
import ohos.aafwk.content.Intent;
/**
 * Created by CaoDongping on 4/7/16.
 */
@Router(value = "home/:homeName", stringParams = "o")
public class HomeAbility extends DumpExtrasAbility {
    /**
     * 定义一个 用于接收的变量
     */
    public static final int RESULT_OK = -1;

    @Override
    protected void onActive() {
        super.onActive();
        Intent intent = new Intent();
        intent.setParam("msg","goodbye");
        setResult(RESULT_OK, intent);
    }
}
