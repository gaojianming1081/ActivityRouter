package com.github.mzule.abilityrouter.slice;

import com.github.mzule.abilityrouter.ResourceTable;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Text;
import ohos.agp.utils.TextAlignment;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.utils.net.Uri;
/**
 * Created by CaoDongping on 8/9/16.
 */
public class ErrorStackAbility extends Ability {
    /**
     * 跳转
     *
     * @param context 上下文
     * @param uri 地址
     * @param e1 错误日志
     * @return makeIntent 跳转的方法
     */
    public static Intent makeIntent(Context context, Uri uri, Throwable e1) {
        Intent intent1 = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withBundleName(context.getBundleName())
                .withFlags(Intent.FLAG_ABILITY_NEW_MISSION)
                .withAbilityName(ErrorStackAbility.class.getName())
                .build();
        intent1.setParam("uri",uri);
        intent1.setParam("error",e1);
        intent1.setOperation(operation);
        return intent1;
    }

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_error);
        Text error = (Text) findComponentById(ResourceTable.Id_error_text);
        Throwable e1 = (Throwable)getIntent().getSerializableParam("error");
        Uri uri = getIntent().getParcelableParam("uri");
        error.setText(String.format("Error on open uri %s\n", uri));
        error.append(HiLog.getStackTrace(e1));
        error.setTextAlignment(TextAlignment.START);
    }
}
