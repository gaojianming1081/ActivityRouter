/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.github.mzule.abilityrouter;

import com.github.mzule.abilityrouter.annotation.Modules;
import com.github.mzule.abilityrouter.router.RouterCallback;
import com.github.mzule.abilityrouter.router.RouterCallbackProvider;
import com.github.mzule.abilityrouter.router.SimpleRouterCallback;
import com.github.mzule.abilityrouter.slice.ErrorStackAbility;
import com.github.mzule.abilityrouter.slice.LaunchAbility;
import com.github.mzule.abilityrouter.slice.NotFoundAbility;
import ohos.aafwk.ability.AbilityPackage;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.app.Context;
import ohos.utils.net.Uri;
/**
 * MyApplication
 *
 * @since 2021-04-26
 **/
@Modules({"app", "sdk"})
public class MyApplication extends AbilityPackage implements RouterCallbackProvider {
    private static final int beforopenmark = 11;
    private static final int notFound = 2;
    @Override
    public void onInitialize() {
        super.onInitialize();
    }

    @Override
    public RouterCallback provideRouterCallback() {
        return new SimpleRouterCallback() {

            @Override
            public boolean beforeOpen(Context context, Uri uri) {
                if (uri.toString().startsWith("mzule://")) {
                    Intent intent = new Intent();
                    Operation operationBuilder = new Intent.OperationBuilder()
                            .withBundleName(context.getBundleName())
                            .withAbilityName(LaunchAbility.class.getName())
                            .build();
                    intent.setOperation(operationBuilder);
                    context.startAbility(intent,beforopenmark);
                    return true;
                }
                return false;
            }

            @Override
            public void notFound(Context context, Uri uri) {
                Intent intent = new Intent();
                Operation operation = new Intent.OperationBuilder()
                        .withBundleName(context.getBundleName())
                        .withFlags(Intent.FLAG_ABILITY_NEW_MISSION)
                        .withAbilityName(NotFoundAbility.class.getName())
                        .build();
                intent.setOperation(operation);
                context.startAbility(intent,notFound);
            }

            @Override
            public void error(Context context, Uri uri, Throwable e) {
                context.startAbility(ErrorStackAbility.makeIntent(context,uri,e),1);
            }
        };
    }
}
